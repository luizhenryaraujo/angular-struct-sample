import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

export const routes: Routes = [
{
  path: 'users',
  loadChildren: './users/users.module#UsersModule',
},
{
  path: 'posts',
  loadChildren: './posts/posts.module#PostsModule',
},
{
  path: '**',
  redirectTo: '/'
}];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
