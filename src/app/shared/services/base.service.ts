import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { QueryParams } from '../models/query-params';

@Injectable({
  providedIn: 'root'
})

export class BaseService<T> {

  protected readonly base_url = environment.baseUrl;
  constructor(
    public http: HttpClient,
    public endpoint: string,
  ) { }

  public all(params?: any): Observable<T[]> {
    const _param = QueryParams.query(params);
    return this.http.get<T[]>(`${this.base_url}${this.endpoint}`, {params: _param});
  }

  public create(data: T): Observable<T> {
    return this.http.post<T>(`${this.base_url}${this.endpoint}`, data);
  }

  public update(data: T): Observable<T> {
    return this.http.put<T>(`${this.base_url}${this.endpoint}`, data);
  }

  public partial(data: T): Observable<T> {
    return this.http.patch<T>(`${this.base_url}${this.endpoint}`, data);
  }

  public remove(id: number): Observable<any> {
    return this.http.delete<T>(`${this.base_url}${this.endpoint}/${id}`);
  }
}
