import { IAddress } from './address.interface';
import { IGeo } from './geo.interface';
import { ICompany } from './company.interface';

export {
  IAddress,
  IGeo,
  ICompany
};
