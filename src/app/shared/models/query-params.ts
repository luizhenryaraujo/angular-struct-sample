import { HttpParams } from '@angular/common/http';

export class QueryParams {
  static query(params: any) {
    let _params = new HttpParams();
    if (params) {
      for (const [key, value] of Object.entries(params)) {
        if (!!value) {
          //@ts-ignore
          _params = _params.append(key, value);
        }
      }
    }
    return _params;
  }

  static objectToURL(data: any) {

    const query = key => {
       const value = data[key];
       return `${key}=${encodeURIComponent(value)}`;
    };

    return Object.keys(data)
      .filter(key => !!(data[key]))
      .map(query).join('&');
  }
}