import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { IPosts } from '@app/posts/interfaces/posts.interface';
import { PostsState } from '../../store/posts.state';
import { GetPosts } from '../../store/posts.actions';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.scss']
})
export class PostsListComponent implements OnInit {

  @Select(PostsState.getPostsList) posts$: Observable<IPosts[]>;
  constructor(
    private store: Store,
  ) {}

  ngOnInit() {
    this.store.dispatch(new GetPosts());
  }

}
