import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { PostsListComponent } from './components/posts-list/posts-list.component';
import { PostsComponent } from './container/posts.component';

import { PostsRoutes } from './posts.routing';

@NgModule({
  declarations: [
    PostsListComponent,
    PostsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(PostsRoutes)
  ]
})
export class PostsModule { }
