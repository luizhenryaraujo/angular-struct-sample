import { HttpClient } from '@angular/common/http';
import { BaseService } from '@shared/services/base.service';
import { Injectable } from '@angular/core';
import { IPosts } from './interfaces/posts.interface';

@Injectable({
  providedIn: 'root'
})
export class PostsService extends BaseService<IPosts> {

  constructor(
    http: HttpClient,
  ) {
    super(http, 'posts');
  }
}
