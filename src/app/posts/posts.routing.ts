import { PostsListComponent } from '@app/posts/components/posts-list/posts-list.component';
import { PostsComponent } from './container/posts.component';
import { Routes } from '@angular/router';

export const PostsRoutes: Routes = [
  {
    path: '',
    component: PostsComponent,
    children: [
      {
        path: '',
        component: PostsListComponent,
        data: {
          title: 'Posts',
          urls: [{ title: 'Posts', url: '/posts' }, { title: 'Posts List' }]
        }
      },
    ]
  }
];
