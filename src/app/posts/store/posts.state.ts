import { State, Action, StateContext, Selector } from '@ngxs/store';
import { GetPosts } from './posts.actions';
import { Posts } from './../models/posts';
import { PostsService } from './../posts.service';
import {tap} from 'rxjs/operators';

export class PostsStateModel {
  posts: Posts[];
}

@State<PostsStateModel>({
  name: 'posts',
  defaults: {
    posts: [],
  }
})


export class PostsState {

  constructor(
    private service: PostsService
  ) {}

  @Selector()
  static getPostsList(state: PostsStateModel) {
    return state.posts;
  }

  @Action(GetPosts)
  getPosts({getState, setState}: StateContext<PostsStateModel>) {
    return this.service.all().pipe(
      tap((result) => {
        const state = getState();
        setState({
          ...state,
          posts: result
        });
      })
    );
  }
}
