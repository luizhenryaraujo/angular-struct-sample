import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { UsersListComponent } from './components/users-list/users-list.component';
import { UsersComponent } from './container/users.component';

import { UsersRoutes } from './users.routing';
import { UsersDetailComponent } from './components/users-detail/users-detail.component';

@NgModule({
  declarations: [
    UsersListComponent,
    UsersComponent,
    UsersDetailComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(UsersRoutes),
  ]
})
export class UsersModule { }
