import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '@shared/services/base.service';
import { IUsers } from './interfaces/users.interface';

@Injectable({
  providedIn: 'root'
})
export class UsersService extends BaseService<IUsers> {

    constructor(http: HttpClient) {
    super(http, 'users');
  }
}
