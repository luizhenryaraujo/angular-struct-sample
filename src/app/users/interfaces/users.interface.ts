import {
  IAddress,
  ICompany,
} from '@shared/interfaces/';

export interface IUsers {
  id: number;
  name: string;
  username: string;
  address: IAddress;
  phone: string;
  website: string;
  company: ICompany;
}
