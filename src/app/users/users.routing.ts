import { Routes } from '@angular/router';
import { UsersComponent } from './container/users.component';
import { UsersListComponent } from './components/users-list/users-list.component';

export const UsersRoutes: Routes = [
  {
    path: '',
    component: UsersComponent,
    children: [
      {
        path: '',
        component: UsersListComponent,
        data: {
          title: 'Users',
          urls: [{ title: 'Users', url: '/users' }, { title: 'Users List' }]
        }
      },
    ]
  }
];
