import { State, Action, StateContext, Selector } from '@ngxs/store';
import { GetUsers, DeleteUsers } from './users.action';
import { Users } from './../models/users';
import { UsersService } from './../users.service';
import { tap } from 'rxjs/operators';

export class UsersStateModel {
  users: Users[];
}

@State<UsersStateModel>({
  name: 'users',
  defaults: {
    users: [],
  }
})


export class UsersState {

  constructor(
    private service: UsersService
  ) {}

  @Selector()
  static getUsersList(state: UsersStateModel) {
    return state.users;
  }

  @Action(GetUsers)
  getUsers({getState, setState}: StateContext<UsersStateModel>) {
    return this.service.all().pipe(
      tap((result) => {
        const state = getState();
        setState({
          ...state,
          users: result
        });
      })
    );
  }

  @Action(DeleteUsers)
  deleteTodo({getState, setState}: StateContext<UsersStateModel>, {id}: DeleteUsers) {
    return this.service.remove(id).pipe(
      tap(() => {
        const state = getState();
        const filteredUsers = state.users.filter(item => item.id !== id);
        setState({
          ...state,
          users: filteredUsers,
        });
      })
    );
  }
}
