export class GetUsers {
  static readonly type = '[Users] List';
}

export class DeleteUsers {
  static readonly type = '[Users] Deleted';

  constructor(public id: number) {}
}
