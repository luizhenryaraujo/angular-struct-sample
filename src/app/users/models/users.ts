export class Users {
  id: number;
  name: string;
  username: string;
  address: any;
  phone: string;
  website: string;
  company: any;
}
