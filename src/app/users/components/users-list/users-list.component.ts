import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { IUsers } from '@app/users/interfaces/users.interface';
import { GetUsers, DeleteUsers } from '../../store/users.action';
import { UsersState } from '../../store/users.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  @Select(UsersState.getUsersList) users$: Observable<IUsers[]>;

  constructor(
    private store: Store,
  ) { }

  ngOnInit() {
    this.store.dispatch(new GetUsers());
  }

  public remove(id: number) {
    console.log(id);
    this.store.dispatch(new DeleteUsers(id));
  }
}
